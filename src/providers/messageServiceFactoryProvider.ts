import {Services} from "kontik";
import MessageFactory from "../services/MessageService/factories/MessageFactory";
import MessageRepository from "../services/MessageService/repositories/MessageRepository";
import ExistingMessageFactory from "../services/MessageService/factories/ExistingMessageFactory";
import MessageServiceFactory from "../services/MessageService/factories/MessageServiceFactory";
import UserServiceFactory from "../services/UserService/factories/UserServiceFactory";
import JwtService from "../services/JwtService/JwtService";

export default async (services: Services, config: any) => {
    const repository = new MessageRepository(config)

    const userServiceFactory = await services.getService<UserServiceFactory>('userServiceFactoryProvider')
    const userService = await userServiceFactory.create()

    const jwtService = await services.getService<JwtService>('jwtServiceProvider')

    const existingUserFactory = new ExistingMessageFactory(
        new MessageFactory()
    )

    return new MessageServiceFactory(repository, repository, existingUserFactory, userService, jwtService, config)
}
