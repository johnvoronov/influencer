import {Services} from "kontik";
import ImageService from "../services/ImageService/ImageService";

export default async (services: Services, config: any) => {
    return new ImageService(
        config.media.max_uploaded_size
    )
}