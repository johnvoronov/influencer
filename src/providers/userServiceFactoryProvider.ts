import {Services} from "kontik";
import UserServiceFactory from "../services/UserService/factories/UserServiceFactory";
import UserRepository from "../services/UserService/repositories/UserRepository";
import ExistingUserFactory from "../services/UserService/factories/ExistingUserFactory";
import UserFactory from "../services/UserService/factories/UserFactory";

export default async (services: Services, config: any) => {
    const repository = new UserRepository(config)
    const existingUserFactory = new ExistingUserFactory(
        new UserFactory()
    )

    return new UserServiceFactory(repository, repository, existingUserFactory, config)
}
