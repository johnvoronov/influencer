import {Context} from "koa";
import {Services} from "kontik";
import AbstractRoute from "../../services/HttpService/routes/AbstractRoute";
import JsonResponse from "../../services/HttpService/responses/JsonResponse";
import AbstractResponse from "../../services/HttpService/responses/AbstractResponse";
import RouteConstructParams from "../../services/HttpService/models/RouteConstructParams";
import MessageServiceFactory from "../../services/MessageService/factories/MessageServiceFactory";
import MessageListData from "../../services/MessageService/interfaces/MessageListData";
import MessageAccessDeniedError from "../../services/MessageService/errors/MessageAccessDeniedError";

class GetMessageList extends AbstractRoute {
    protected services: Services

    constructor(routeConstructParams: RouteConstructParams) {
        super()
        this.services = routeConstructParams.services
    }

    get method(): string {
        return "POST";
    }

    get path(): string {
        return "/v1/message/list";
    }

    async handler(ctx: Context): Promise<AbstractResponse> {
        const messageServiceFactory = await this.services.getService<MessageServiceFactory>('messageServiceFactoryProvider')
        const messageService = await messageServiceFactory.create()

        let token = ctx.request.header['authorization']
        if (!token) {
            return new JsonResponse({
                error: 'access_denied',
                error_description: 'Message access denied.'
            }, 403);
        }

        token = token.replace('Bearer ', '');

        const {sender, recipient} = ctx.request.body
        let messageList: MessageListData

        try {
            messageList = await messageService.getList(token, sender, recipient)
            console.log(messageList)
        } catch (e) {
            if (e instanceof MessageAccessDeniedError) {
                return new JsonResponse({
                    error: 'access_denied',
                    error_description: 'Message access denied.'
                }, 404);
            }

            return new JsonResponse({
                error: 'internal_server_error',
                error_description: 'Internal server error.'
            }, 500);
        }

        return new JsonResponse({
            messageList
        }, 200);
    }
}

export default async (routeConstructParams: RouteConstructParams) => {
    return new GetMessageList(routeConstructParams)
}

