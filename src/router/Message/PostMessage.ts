import {Context} from "koa";
import {Services} from "kontik";
import AbstractRoute from "../../services/HttpService/routes/AbstractRoute";
import JsonResponse from "../../services/HttpService/responses/JsonResponse";
import AbstractResponse from "../../services/HttpService/responses/AbstractResponse";
import RouteConstructParams from "../../services/HttpService/models/RouteConstructParams";
import JwtService from "../../services/JwtService/JwtService";
import MessageServiceFactory from "../../services/MessageService/factories/MessageServiceFactory";
import MessageAccessDeniedError from "../../services/MessageService/errors/MessageAccessDeniedError";
import Message from "../../services/MessageService/models/Message";

class PostMessage extends AbstractRoute {
    protected services: Services

    constructor(routeConstructParams: RouteConstructParams) {
        super()
        this.services = routeConstructParams.services
    }

    get method(): string {
        return "POST";
    }

    get path(): string {
        return "/v1/message";
    }

    async handler(ctx: Context): Promise<AbstractResponse> {
        const messageServiceFactory = await this.services.getService<MessageServiceFactory>('messageServiceFactoryProvider')
        const messageService = await messageServiceFactory.create()

        const jwtService = await this.services.getService<JwtService>('jwtServiceProvider')
        let token = ctx.request.header['authorization']
        if (!token) {
            return new JsonResponse({
                error: 'access_denied',
                error_description: 'Message access denied.'
            }, 403);
        }

        token = token.replace('Bearer ', '');

        const { userId } = await jwtService.getJwtData(token)

        const {recipient, pair, text} = ctx.request.body

        let message = new Message(userId, recipient, pair, text)

        try {
            message = await messageService.send(message)
        } catch (e) {
            if (e instanceof MessageAccessDeniedError) {
                return new JsonResponse({
                    error: 'access_denied',
                    error_description: 'Message access denied.'
                }, 403);
            }

            return new JsonResponse({
                error: 'internal_server_error',
                error_description: 'Internal server error.'
            }, 500);
        }

        return new JsonResponse({
            message
        }, 200);
    }
}

export default async (routeConstructParams: RouteConstructParams) => {
    return new PostMessage(routeConstructParams)
}

