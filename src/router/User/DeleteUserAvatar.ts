import {Context} from "koa";
import {Services} from "kontik";
import AbstractRoute from "../../services/HttpService/routes/AbstractRoute";
import JsonResponse from "../../services/HttpService/responses/JsonResponse";
import AbstractResponse from "../../services/HttpService/responses/AbstractResponse";
import RouteConstructParams from "../../services/HttpService/models/RouteConstructParams";
import UserServiceFactory from "../../services/UserService/factories/UserServiceFactory";
import ExistingUser from "../../services/UserService/models/ExistingUser";
import JwtService from "../../services/JwtService/JwtService";
import UserIdNotFoundError from "../../services/UserService/errors/UserIdNotFoundError";

class DeleteUserAvatar extends AbstractRoute {
    protected services: Services

    constructor(routeConstructParams: RouteConstructParams) {
        super()
        this.services = routeConstructParams.services
    }

    get method(): string {
        return "DELETE";
    }

    get path(): string {
        return "/v1/user/avatar";
    }

    async handler(ctx: Context): Promise<AbstractResponse> {
        const userServiceFactory = await this.services.getService<UserServiceFactory>('userServiceFactoryProvider')
        const userService = await userServiceFactory.create()

        const jwtService = await this.services.getService<JwtService>('jwtServiceProvider')
        let token = ctx.request.header['authorization']
        if (!token) {
            return new JsonResponse({
                error: 'access_denied',
                error_description: 'Message access denied.'
            }, 403);
        }

        token = token.replace('Bearer ', '');

        const { userId } = await jwtService.getJwtData(token)

        let user: ExistingUser

        try {
            user = await userService.getUserById(userId)
        } catch (e) {
            if (e instanceof UserIdNotFoundError) {
                return new JsonResponse({
                    error: 'not_found',
                    error_description: 'Provided user id was not found.'
                }, 404);
            }

            return new JsonResponse({
                error: 'internal_server_error',
                error_description: 'Internal server error.'
            }, 500);
        }

        await userService.updateUserAvatar(userId, '', '')

        return new JsonResponse({
            message: "User avatar was successful deleted"
        }, 200);
    }
}

export default async (routeConstructParams: RouteConstructParams) => {
    return new DeleteUserAvatar(routeConstructParams)
}

