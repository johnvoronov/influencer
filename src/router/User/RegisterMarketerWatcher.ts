import {Context} from "koa";
import {Services} from "kontik";
import AbstractRoute from "../../services/HttpService/routes/AbstractRoute";
import JsonResponse from "../../services/HttpService/responses/JsonResponse";
import AbstractResponse from "../../services/HttpService/responses/AbstractResponse";
import RouteConstructParams from "../../services/HttpService/models/RouteConstructParams";
import UserServiceFactory from "../../services/UserService/factories/UserServiceFactory";
import UnknownUserTypeError from "../../services/UserService/errors/UnknownUserTypeError";
import MarketerWatcherUser from "../../services/UserService/models/MarketerWatcherUser";
import EmailAlreadyInUseError from "../../services/UserService/errors/EmailAlreadyInUseError";
import UserRegistrationError from "../../services/UserService/errors/UserRegistrationError";
import JwtService from "../../services/JwtService/JwtService";
import UserIdNotFoundError from "../../services/UserService/errors/UserIdNotFoundError";

class RegisterMarketerWatcher extends AbstractRoute {
    protected services: Services

    constructor(routeConstructParams: RouteConstructParams) {
        super()
        this.services = routeConstructParams.services
    }

    get method(): string {
        return "POST";
    }

    get path(): string {
        return "/v1/registration/marketer-watcher";
    }

    async handler(ctx: Context): Promise<AbstractResponse> {
        const userServiceFactory = await this.services.getService<UserServiceFactory>('userServiceFactoryProvider')
        const userService = await userServiceFactory.create()

        const jwtService = await this.services.getService<JwtService>('jwtServiceProvider')
        let token = ctx.request.header['authorization']
        if (!token) {
            return new JsonResponse({
                error: 'access_denied',
                error_description: 'Message access denied.'
            }, 403);
        }

        token = token.replace('Bearer ', '');

        const { userId: marketer } = await jwtService.getJwtData(token)

        try {
            await userService.getUserById(marketer)
        } catch (e) {
            console.log(1231, e)
            if (e instanceof UserIdNotFoundError) {
                return new JsonResponse({
                    error: 'not_found',
                    error_description: 'Provided user id was not found.'
                }, 404);
            }

            return new JsonResponse({
                error: 'internal_server_error',
                error_description: 'Internal server error.'
            }, 500);
        }

        const {email, firstName, lastName, password} = ctx.request.body;

        const marketerUser = new MarketerWatcherUser(email, marketer, firstName, lastName)

        try {
            await userService.register(marketerUser, password)
        } catch (e) {
            if (e instanceof UnknownUserTypeError) {
                return new JsonResponse({
                    error: 'unknown_user_type',
                    error_description: 'Provided type are invalid.'
                }, 401);
            }

            if (e instanceof EmailAlreadyInUseError) {
                return new JsonResponse({
                    error: 'email_already_in_use',
                    error_description: 'Email already in use.'
                }, 401);
            }

            if (e instanceof UserRegistrationError) {
                return new JsonResponse({
                    error: 'user_registration',
                    error_description: 'Unknown user registration.'
                }, 400);
            }

            return new JsonResponse({
                error: 'internal_server_error',
                error_description: 'Internal server error.'
            }, 500);
        }

        return new JsonResponse({
            message: 'New marketer watcher is successfully registered'
        }, 201);
    }
}

export default async (routeConstructParams: RouteConstructParams) => {
    return new RegisterMarketerWatcher(routeConstructParams)
}

