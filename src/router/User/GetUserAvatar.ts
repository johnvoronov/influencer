import {Context} from "koa";
import {Services} from "kontik";
import AbstractRoute from "../../services/HttpService/routes/AbstractRoute";
import JsonResponse from "../../services/HttpService/responses/JsonResponse";
import AbstractResponse from "../../services/HttpService/responses/AbstractResponse";
import RouteConstructParams from "../../services/HttpService/models/RouteConstructParams";
import UserServiceFactory from "../../services/UserService/factories/UserServiceFactory";
import ExistingUser from "../../services/UserService/models/ExistingUser";
import UserIdNotFoundError from "../../services/UserService/errors/UserIdNotFoundError";

class GetUserAvatar extends AbstractRoute {
    protected services: Services

    constructor(routeConstructParams: RouteConstructParams) {
        super()
        this.services = routeConstructParams.services
    }

    get method(): string {
        return "GET";
    }

    get path(): string {
        return "/v1/user/avatar/:userId";
    }

    async handler(ctx: Context): Promise<AbstractResponse> {
        const userServiceFactory = await this.services.getService<UserServiceFactory>('userServiceFactoryProvider')
        const userService = await userServiceFactory.create()

        const {userId} = ctx.params

        let user: ExistingUser

        try {
            user = await userService.getUserById(userId)
        } catch (e) {
            if (e instanceof UserIdNotFoundError) {
                return new JsonResponse({
                    error: 'not_found',
                    error_description: 'Provided user id was not found.'
                }, 404);
            }

            return new JsonResponse({
                error: 'internal_server_error',
                error_description: 'Internal server error.'
            }, 500);
        }

        return new JsonResponse({
            userId: user.userId,
            avatar: user.user.avatar,
            avatarDescription: user.user.avatarDescription
        }, 200);
    }
}

export default async (routeConstructParams: RouteConstructParams) => {
    return new GetUserAvatar(routeConstructParams)
}

