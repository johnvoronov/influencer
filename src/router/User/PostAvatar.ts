import * as multiparty from 'multiparty'
import * as fs from 'fs'
import AbstractRoute from "../../services/HttpService/routes/AbstractRoute";
import {Context} from "koa";
import JsonResponse from "../../services/HttpService/responses/JsonResponse";
import EmptyResponse from "../../services/HttpService/responses/EmptyResponse";
import ImageService from "../../services/ImageService/ImageService";
import {IncomingMessage} from "http";
import RouteConstructParams from "../../services/HttpService/models/RouteConstructParams";
import MaximumImageSizeError from "../../services/ImageService/errors/MaximumImageSizeError";
import UserServiceFactory from "../../services/UserService/factories/UserServiceFactory";
import JwtService from "../../services/JwtService/JwtService";
import {Services} from "kontik";

class NoImageFoundError extends Error {
    constructor() {
        super('No image was found in request')

        Object.setPrototypeOf(this, NoImageFoundError.prototype)
    }
}

class PostAvatar extends AbstractRoute {
    protected services: Services

    constructor(
        protected readonly imageService: ImageService,
        routeConstructParams: RouteConstructParams
    ) {
        super()
        this.services = routeConstructParams.services
    }

    readonly method: string = 'POST'
    readonly path: string = '/v1/user/avatar'

    async handler(ctx: Context): Promise<EmptyResponse> {
        const userServiceFactory = await this.services.getService<UserServiceFactory>('userServiceFactoryProvider')
        const userService = await userServiceFactory.create()

        const jwtService = await this.services.getService<JwtService>('jwtServiceProvider')

        const {description} = ctx.request.body
        let token = ctx.request.header['authorization']
        if (token) {
            token = token.replace('Bearer ', '');
        }

        const { userId } = await jwtService.getJwtData(token)

        try {
            const reqImageData = await this.getImageDataFromContext(ctx.req)
            const avatarName = `${(new Date()).getTime()}_${reqImageData.filename}`
            const imageData = await this.imageService.saveImage(avatarName, reqImageData.data)

            await userService.updateUserAvatar(userId, avatarName, description)
            return new EmptyResponse()
        } catch (e) {
            if (e instanceof NoImageFoundError) {
                return new JsonResponse({
                    error: 'invalid_request',
                    error_description: e.message
                }, 400)
            }

            if (e.message === 'maximum file length exceeded' || e instanceof MaximumImageSizeError) {
                return new JsonResponse({
                    error: 'invalid_request',
                    error_description: `Maximum image size is ${this.imageService.MAXIMUM_IMAGE_SIZE}`
                }, 413)
            }

            throw e
        }
    }

    async getImageDataFromContext(req: IncomingMessage): Promise<{filename: string, data: Buffer}> {
        return new Promise((resolve, reject) => {
            const form = new multiparty.Form({
                maxFilesSize: this.imageService.MAXIMUM_IMAGE_SIZE
            })

            form.parse(req, (err: any, fields: any, files: any) => {
                if (err && err.message === 'stream ended unexpectedly') {
                    return reject(new NoImageFoundError())
                }

                if (err) {
                    return reject(err)
                }

                if (!files['image']) {
                    return reject(new NoImageFoundError())
                }

                fs.readFile(files['image'][0]['path'], (err, data) => {
                    if (err) {
                        return reject(err)
                    }

                    resolve({ data, filename: files['image'][0]['originalFilename'] })
                })
            })
        })
    }
}

export default async (routeParams: RouteConstructParams) => {
    const imageService = await routeParams.services.getService<ImageService>('imageServiceProvider')

    return new PostAvatar(imageService, routeParams)
}
