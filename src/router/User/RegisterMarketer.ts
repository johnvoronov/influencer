import {Context} from "koa";
import {Services} from "kontik";
import AbstractRoute from "../../services/HttpService/routes/AbstractRoute";
import JsonResponse from "../../services/HttpService/responses/JsonResponse";
import AbstractResponse from "../../services/HttpService/responses/AbstractResponse";
import RouteConstructParams from "../../services/HttpService/models/RouteConstructParams";
import UserServiceFactory from "../../services/UserService/factories/UserServiceFactory";
import UnknownUserTypeError from "../../services/UserService/errors/UnknownUserTypeError";
import MarketerUser from "../../services/UserService/models/MarketerUser";
import EmailAlreadyInUseError from "../../services/UserService/errors/EmailAlreadyInUseError";
import UserRegistrationError from "../../services/UserService/errors/UserRegistrationError";

class RegisterMarketer extends AbstractRoute {
    protected services: Services

    constructor(routeConstructParams: RouteConstructParams) {
        super()
        this.services = routeConstructParams.services
    }

    get method(): string {
        return "POST";
    }

    get path(): string {
        return "/v1/registration/marketer";
    }

    async handler(ctx: Context): Promise<AbstractResponse> {
        const userServiceFactory = await this.services.getService<UserServiceFactory>('userServiceFactoryProvider')
        const userService = await userServiceFactory.create()

        const {email, firstName, lastName, password} = ctx.request.body;

        const marketerUser = new MarketerUser(email, firstName, lastName)

        try {
            await userService.register(marketerUser, password)
        } catch (e) {
            if (e instanceof UnknownUserTypeError) {
                return new JsonResponse({
                    error: 'unknown_user_type',
                    error_description: 'Provided type are invalid.'
                }, 401);
            }

            if (e instanceof EmailAlreadyInUseError) {
                return new JsonResponse({
                    error: 'email_already_in_use',
                    error_description: 'Email already in use.'
                }, 401);
            }

            if (e instanceof UserRegistrationError) {
                return new JsonResponse({
                    error: 'user_registration',
                    error_description: 'Unknown user registration.'
                }, 400);
            }

            return new JsonResponse({
                error: 'internal_server_error',
                error_description: 'Internal server error.'
            }, 500);
        }

        return new JsonResponse({
            message: 'New marketer is successfully registered'
        }, 201);
    }
}

export default async (routeConstructParams: RouteConstructParams) => {
    return new RegisterMarketer(routeConstructParams)
}

