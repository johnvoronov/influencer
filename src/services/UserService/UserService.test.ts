import {expect} from 'chai'
import * as sinon from 'sinon'
import UserService from "./UserService";
import UserData from "./interfaces/UserData";
import InfluencerUser from "./models/InfluencerUser";
import {GENDERS, USER_TYPES} from "./entities/enums";
import MarketerUser from "./models/MarketerUser";
import MarketerWatcherUser from "./models/MarketerWatcherUser";
import ExistingUserData from "./interfaces/ExistingUserData";
import NewUserData from "./interfaces/NewUserData";
import ExistingUser from "./models/ExistingUser";
import AbstractUser from "./models/AbstractUser";
import UserEmailNotFoundError from "./errors/UserEmailNotFoundError";
import UnknownUserTypeError from "./errors/UnknownUserTypeError";

describe('UserService', function () {
    it ('should save correct influencer user type to repository', async function () {
        const FAKE_USER_REPOSITORY_WRITER: any = {
            saveUser: sinon.spy(async (userData: UserData) => {})
        }

        const userService = new UserService({} as any, FAKE_USER_REPOSITORY_WRITER, {} as any, {} as any)

        const FAKE_INFLUENCER = new InfluencerUser('john@doe.com', 'John', 'Doe', new Date('1980-01-01'), GENDERS.MAN)

        await userService.register(FAKE_INFLUENCER, 'verySecretMuchWow')

        expect(FAKE_USER_REPOSITORY_WRITER.saveUser.calledOnce).to.be.eql(true)
        expect(FAKE_USER_REPOSITORY_WRITER.saveUser.args[0][0].type).to.be.eql(USER_TYPES.INFLUENCER)
    })

    it ('should save correct marketer user type to repository', async function () {
        const FAKE_USER_REPOSITORY_WRITER: any = {
            saveUser: sinon.spy(async (userData: UserData) => {})
        }

        const userService = new UserService({} as any, FAKE_USER_REPOSITORY_WRITER, {} as any, {} as any)

        const FAKE_MARKETER = new MarketerUser('john@doe.cz', 'John', 'Doe')

        await userService.register(FAKE_MARKETER, 'verySecretMuchWow')

        expect(FAKE_USER_REPOSITORY_WRITER.saveUser.calledOnce).to.be.eql(true)
        expect(FAKE_USER_REPOSITORY_WRITER.saveUser.args[0][0].type).to.be.eql(USER_TYPES.MARKETER)
    })

    it ('should save correct marketer watcher user type to repository', async function () {
        const FAKE_USER_REPOSITORY_WRITER: any = {
            saveUser: sinon.spy(async (userData: UserData) => {})
        }
        const userService = new UserService({} as any, FAKE_USER_REPOSITORY_WRITER, {} as any, {} as any)

        const FAKE_MARKETER_WATCHER = new MarketerWatcherUser('1', 'john@doe.cz')

        await userService.register(FAKE_MARKETER_WATCHER, 'verySecretMuchWow')

        expect(FAKE_USER_REPOSITORY_WRITER.saveUser.calledOnce).to.be.eql(true)
        expect(FAKE_USER_REPOSITORY_WRITER.saveUser.args[0][0].type).to.be.eql(USER_TYPES.MARKETER_WATCHER)
    })

    it('should return user by credentials', async function () {
        const FAKE_USER = new ExistingUser(
            Math.random().toString(36).substring(7),
            new MarketerWatcherUser('1', 'john@doe.com')
        )

        const FAKE_EXISTING_USER_FACTORY: any = {
            create: sinon.spy(async () => FAKE_USER)
        }

        const FAKE_USER_REPOSITORY_READER: any = {
            // getUserByCredentials(email: string, password: string): Promise<ExistingUserData>
            getUserByCredentials: sinon.spy(async (email: string, password: string) => {})
        }

        const service = new UserService(FAKE_USER_REPOSITORY_READER, {} as any, FAKE_EXISTING_USER_FACTORY, {} as any)

        const existingUser = await service.getUserByCredentials('john@doe.com', 'superSecret')

        expect(existingUser).to.be.instanceof(ExistingUser)
        expect(existingUser).to.be.equal(FAKE_USER)
    })

    it('should return user by id', async function () {
        const FAKE_USER = new ExistingUser(
            Math.random().toString(36).substring(7),
            new MarketerWatcherUser('1', 'john@doe.com')
        )

        const FAKE_EXISTING_USER_FACTORY: any = {
            create: sinon.spy(async () => FAKE_USER)
        }

        const FAKE_USER_REPOSITORY_READER: any = {
            getUserById: sinon.spy(async (userId: string) => {})
        }

        const service = new UserService(FAKE_USER_REPOSITORY_READER, {} as any, FAKE_EXISTING_USER_FACTORY, {})

        const existingUser = await service.getUserById(FAKE_USER.userId)

        expect(existingUser).to.be.instanceof(ExistingUser)
        expect(existingUser).to.be.equal(FAKE_USER)
    })

    it ('should throw error when try to register unknown user type', async function () {
        const FAKE_USER_REPOSITORY_WRITER: any = {
            saveUser: sinon.spy(async (userData: UserData) => {})
        }

        const userService = new UserService({} as any, FAKE_USER_REPOSITORY_WRITER, {} as any, {} as any)

        class FakeUserType extends AbstractUser {
            // @ts-ignore
            readonly type: 'fake-type';
        }

        const FAKE_USER_TYPE = new FakeUserType('1', 'fake@email.com')

        try {
            // @ts-ignore
            await userService.register(FAKE_USER_TYPE, 'verySecretMuchWow')
        } catch (e) {
            expect(e).to.be.instanceof(UnknownUserTypeError)
            return
        }

        throw new Error('should never happened')
    })
})
