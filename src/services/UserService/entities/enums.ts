export enum GENDERS {
    MAN = 'man',
    WOMAN = 'woman'
}

export enum USER_TYPES {
    INFLUENCER = 'influencer',
    MARKETER = 'marketer',
    MARKETER_WATCHER = 'marketer_watcher'
}