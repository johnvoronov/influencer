import ExistingUserData from "./ExistingUserData";

export default interface UserRepositoryReader {
    /**
     *
     * @param email
     * @param password
     * @throws UserEmailNotFoundError
     * @throws MessageRepositoryError
     */
    getUserByCredentials(email: string, password: string): Promise<ExistingUserData>

    /**
     *
     * @param id
     * @throws UserIdNotFoundError
     * @throws MessageRepositoryError
     */
    getUserById(id: string): Promise<ExistingUserData>
}
