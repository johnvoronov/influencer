import UserData from "./UserData";
import ExistingUserData from "./ExistingUserData";

export default interface UserRepositoryWriter {
    /**
     *
     * @param userData
     * @throws EmailAlreadyInUseError
     * @throws MessageRepositoryError
     */
    saveUser(userData: UserData): Promise<ExistingUserData>

    /**
     *
     * @param userId
     * @param userData
     * @throws EmailAlreadyInUseError
     * @throws MessageRepositoryError
     */
    updateUser(userId: string, userData: UserData): Promise<ExistingUserData>
}
