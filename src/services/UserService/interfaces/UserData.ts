import {GENDERS, USER_TYPES} from "../entities/enums";

export default interface UserData {
    readonly type: USER_TYPES
    readonly email: string,
    readonly firstName?: string,
    readonly lastName?: string,
    readonly avatar?: string,
    readonly avatarDescription?: string,
    readonly birthDate?: Date,
    readonly gender?: GENDERS
    readonly jobDescription?: string,
    readonly phoneNumber?: string,
    readonly marketer?: string,
}
