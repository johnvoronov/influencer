import ExistingUserData from "../interfaces/ExistingUserData";
import {USER_TYPES} from "../entities/enums";
import NewUserData from "../interfaces/NewUserData";
import * as crypto from 'crypto'
import UserData from "../interfaces/UserData";
import UserIdNotFoundError from "../errors/UserIdNotFoundError";
import UserRepositoryReader from "../interfaces/UserRepositoryReader";
import UserRepositoryWriter from "../interfaces/UserRepositoryWriter";
import InvalidUserCredentialsError from "../errors/InvalidUserCredentialsError";
import UserEmailNotFoundError from "../errors/UserEmailNotFoundError";
import EmailAlreadyInUseError from "../errors/EmailAlreadyInUseError";

interface PasswordData {
    readonly hash: string,
    readonly salt: string
}

type StoredUser = ExistingUserData & { readonly password: PasswordData }

export default class FakeUserRepository implements UserRepositoryReader, UserRepositoryWriter {
    protected storage: { [id: string]: StoredUser} = {}

    public async saveUser(userData: NewUserData): Promise<ExistingUserData> {
        let userWithSameEmail
        try {
            userWithSameEmail = await this.getStoredUserDataByEmail(userData.email)
        } catch (e) {

        }

        if (userWithSameEmail) {
            throw new EmailAlreadyInUseError(userData.email)
        }

        const id = this.generateUserId()

        const finalData: StoredUser = {
            ...userData,
            id,
            password: this.passwordToSaltHash(userData.password)
        }

        this.storage[id] = finalData

        return this.getExistingUserDataFromStoredData(finalData)
    }

    public async updateUser(userId: string, userData: UserData): Promise<ExistingUserData> {
        const oldData = this.storage[userId]

        if (!oldData) {
            throw new UserIdNotFoundError(userId)
        }

        const newData: StoredUser = {
            ...oldData,
            ...userData
        }

        this.storage[userId] = newData

        return this.getExistingUserDataFromStoredData(newData);
    }

    public async getUserById(id: string): Promise<ExistingUserData> {
        const userData = this.storage[id]

        if (!userData) {
            throw new UserIdNotFoundError(id)
        }

        return userData;
    }

    public async getUserByCredentials(email: string, password: string): Promise<ExistingUserData> {
        const userData = await this.getStoredUserDataByEmail(email)

        if (this.passwordToSaltHash(password, userData.password.salt).hash !== userData.password.hash) {
            throw new InvalidUserCredentialsError()
        }

        return this.getExistingUserDataFromStoredData(userData);
    }

    protected generateUserId(): string {
        return this.getRandomString(10)
    }

    protected getRandomString(length: number): string {
        return crypto.randomBytes(Math.ceil(length/2)).toString('hex').slice(0,length);
    }

    protected passwordToSaltHash(password: string, salt?: string): PasswordData {
        const hashSalt = salt || this.getRandomString(16)
        const hash = crypto.createHmac('sha512', hashSalt);

        hash.update(password);

        const value = hash.digest('hex');

        return {
            salt: hashSalt,
            hash: value
        };
    }

    protected getExistingUserDataFromStoredData(storedUserData: StoredUser): ExistingUserData {
        const existingUser = {
            ...storedUserData
        }

        delete existingUser.password

        return existingUser as ExistingUserData
    }

    protected async getStoredUserDataByEmail(email: string): Promise<StoredUser> {
        const userData = Object.keys(this.storage).find((userId) => this.storage[userId].email === email)

        if (!userData) {
            throw new UserEmailNotFoundError(email)
        }

        return this.storage[userData]
    }
}
