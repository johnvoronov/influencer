import * as crypto from "crypto";
import UserRepositoryWriter from "../interfaces/UserRepositoryWriter";
import UserRepositoryReader from "../interfaces/UserRepositoryReader";
import ExistingUserData from "../interfaces/ExistingUserData";
import UserData from "../interfaces/UserData";
import UserIdNotFoundError from "../errors/UserIdNotFoundError";
import EmailAlreadyInUseError from "../errors/EmailAlreadyInUseError";
import UserRegistrationError from "../errors/UserRegistrationError";
import InvalidUserCredentialsError from "../errors/InvalidUserCredentialsError";
import PgRepository from "../../PgService/repositories/PgRepository";

interface PasswordData {
    readonly hash: string,
    readonly salt: string
}

type StoredUser = ExistingUserData & { readonly password: PasswordData }

export default class UserRepository implements UserRepositoryReader, UserRepositoryWriter  {
    protected readonly pgRepository: PgRepository
    protected storage: { [id: string]: StoredUser} = {}
    protected readonly salt = "123456789";

    constructor(config: any) {
        this.pgRepository = new PgRepository('users', config)
    }

    public async saveUser(userData: any): Promise<ExistingUserData> {
        let response = await this.pgRepository.getDataByParams({ email: userData.email })


        if (response && response[0]) {
            throw new EmailAlreadyInUseError(userData.email)
        }

        // Process password to hash
        userData.password = this.passwordToHash(userData.password)

        response = await this.pgRepository.saveData(userData)

        if (!response) {
            throw new UserRegistrationError(userData)
        }

        return response
    }

    public async updateUser(id: string, userData: UserData): Promise<ExistingUserData> {
        return this.pgRepository.updateData(id, userData)
    }

    public async getUserById(id: string): Promise<ExistingUserData> {
        const response = await this.pgRepository.getDataById(id);

        if (!response) {
            throw new UserIdNotFoundError(id)
        }

        return response
    }

    async getUserByCredentials(email: string, password: string): Promise<ExistingUserData> {
        const hash = this.passwordToHash(password);

        const response = await this.pgRepository.getDataByParams({ email, password: hash })

        if (this.passwordToHash(password) !== response[0].password) {
            throw new InvalidUserCredentialsError()
        }

        return response[0]
    }

    async getUserByEmail(email: string): Promise<ExistingUserData> {
        return this.pgRepository.getDataByParams({ email })
    }

    protected passwordToHash(password: string): string {
        const hash = crypto.createHmac('sha512', this.salt);
        hash.update(password);

        return hash.digest('hex');
    }
}
