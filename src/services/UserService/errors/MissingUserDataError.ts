import UserServiceError from "./UserServiceError";

export default class MissingUserDataError extends UserServiceError {
    constructor() {
        super(`Missing some or all fields in user data necessary for creating user`)

        Object.setPrototypeOf(this, MissingUserDataError.prototype)
    }
}