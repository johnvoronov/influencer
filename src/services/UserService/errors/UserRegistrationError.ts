import UserServiceError from "./UserServiceError";

export default class UserRegistrationError extends UserServiceError {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, UserRegistrationError.prototype)
    }
}