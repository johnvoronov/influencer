import ExistingUserData from "../interfaces/ExistingUserData";
import AbstractUser from "../models/AbstractUser";
import InfluencerUser from "../models/InfluencerUser";
import MissingUserDataError from "../errors/MissingUserDataError";
import MarketerUser from "../models/MarketerUser";
import MarketerWatcherUser from "../models/MarketerWatcherUser";
import {USER_TYPES} from "../entities/enums";
import UnknownUserTypeError from "../errors/UnknownUserTypeError";

export default class UserFactory {
    public async create(userData: ExistingUserData): Promise<AbstractUser> {
        if (userData.type === USER_TYPES.INFLUENCER) {
            return this.createInfluencer(userData)
        }

        if (userData.type === USER_TYPES.MARKETER) {
            return this.createMarketer(userData)
        }

        if (userData.type === USER_TYPES.MARKETER_WATCHER) {
            return this.createMarketerWatcher(userData)
        }

        throw new UnknownUserTypeError(userData.type)
    }

    protected async createInfluencer(userData: ExistingUserData): Promise<InfluencerUser> {
        const {email, firstName, lastName, birthDate, gender, avatar, avatarDescription} = userData


        if (!firstName || !lastName || !birthDate) {
            throw new MissingUserDataError()
        }

        return new InfluencerUser(
            email,
            firstName,
            lastName,
            birthDate,
            gender,
            avatar,
            avatarDescription
        )
    }

    protected async createMarketer(userData: ExistingUserData): Promise<MarketerUser> {
        const {email, firstName, lastName, jobDescription, phoneNumber, avatar, avatarDescription} = userData

        if (!firstName || !lastName) {
            throw new MissingUserDataError()
        }

        return new MarketerUser(
            email,
            firstName,
            lastName,
            jobDescription,
            phoneNumber,
            avatar,
            avatarDescription
        )
    }

    protected async createMarketerWatcher(userData: ExistingUserData): Promise<MarketerWatcherUser> {
        const {email, firstName, lastName, avatar, avatarDescription} = userData

        return new MarketerWatcherUser(
            email,
            firstName,
            lastName,
            avatar,
            avatarDescription
        )
    }
}
