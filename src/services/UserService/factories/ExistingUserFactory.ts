import ExistingUserData from "../interfaces/ExistingUserData";
import ExistingUser from "../models/ExistingUser";
import UserFactory from "./UserFactory";

export default class ExistingUserFactory {
    constructor(
        protected readonly userFactory: UserFactory
    ) {}

    public async create(userData: ExistingUserData): Promise<ExistingUser> {
        return new ExistingUser(
            userData.id,
            await this.userFactory.create(userData)
        )
    }
}
