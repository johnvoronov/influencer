import {expect} from 'chai'
import ExistingUserData from "../interfaces/ExistingUserData";
import {USER_TYPES} from "../entities/enums";
import ExistingUserFactory from "./ExistingUserFactory";
import * as sinon from 'sinon'
import MarketerWatcherUser from "../models/MarketerWatcherUser";

describe('ExistingUserFactory', function () {
    it('should create ExistingUser model from data', async function () {
        const FAKE_USER_DATA: ExistingUserData = {
            id: Math.random().toString(36).substring(7),
            marketer: Math.random().toString(36).substring(7),
            type: USER_TYPES.MARKETER_WATCHER,
            email: 'john@doe.com',
            firstName: 'John',
            lastName: 'Doe'
        }

        const FAKE_USER_FACTORY: any = {
            create: sinon.spy(() => {
                return new MarketerWatcherUser('1', FAKE_USER_DATA.email)
            })
        }

        const factory = new ExistingUserFactory(FAKE_USER_FACTORY)

        const existingUser = await factory.create(FAKE_USER_DATA)

        expect(existingUser.userId).to.be.eql(FAKE_USER_DATA.id)
    })
})
