import {GENDERS, USER_TYPES} from "../entities/enums";
import ExistingUserData from "../interfaces/ExistingUserData";
import UserFactory from "./UserFactory";
import {expect} from 'chai'
import InfluencerUser from "../models/InfluencerUser";
import MissingUserDataError from "../errors/MissingUserDataError";
import MarketerUser from "../models/MarketerUser";
import MarketerWatcherUser from "../models/MarketerWatcherUser";

const FAKE_INFLUENCER_DATA: ExistingUserData = {
    id: Math.random().toString(36).substring(7),
    type: USER_TYPES.INFLUENCER,
    email: 'john@doe.com',
    firstName: 'John',
    lastName: 'Doe',
    birthDate: new Date('1980-01-01'),
    gender: GENDERS.MAN
}

const FAKE_MARKETER_DATA: ExistingUserData = {
    id: Math.random().toString(36).substring(7),
    type: USER_TYPES.MARKETER,
    email: 'john@doe.com',
    firstName: 'John',
    lastName: 'Doe',
    jobDescription: 'He do stuff',
    phoneNumber: '+420777777777'
}

const FAKE_MARKETER_WATCHER_DATA: ExistingUserData = {
    id: Math.random().toString(36).substring(7),
    type: USER_TYPES.MARKETER_WATCHER,
    email: 'john@doe.com',
    firstName: 'John',
    lastName: 'Doe'
}


describe('UserFactory', function () {
    it('should create influencer', async function () {
        const factory = new UserFactory()

        const user = await factory.create(FAKE_INFLUENCER_DATA)

        expect(user).to.be.instanceof(InfluencerUser)
    })

    it('should throw missing data error for influencer', async function () {
        const factory = new UserFactory()

        const FAKE_MISSING_DATA = {
            ...FAKE_INFLUENCER_DATA
        }

        delete FAKE_MISSING_DATA.birthDate

        try {
            const user = await factory.create(FAKE_MISSING_DATA)
        } catch (e) {
            expect(e).to.be.instanceof(MissingUserDataError)
            return
        }

        throw new Error('should never happened')
    })

    it('should create marketer', async function () {
        const factory = new UserFactory()

        const user = await factory.create(FAKE_MARKETER_DATA)

        expect(user).to.be.instanceof(MarketerUser)
    })

    it('should throw missing data error for marketer', async function () {
        const factory = new UserFactory()

        const FAKE_MISSING_DATA = {
            ...FAKE_MARKETER_DATA
        }

        delete FAKE_MISSING_DATA.firstName

        try {
            const user = await factory.create(FAKE_MISSING_DATA)
        } catch (e) {
            expect(e).to.be.instanceof(MissingUserDataError)
            return
        }

        throw new Error('should never happened')
    })


    it('should create marketer watcher', async function () {
        const factory = new UserFactory()

        const user = await factory.create(FAKE_MARKETER_WATCHER_DATA)

        expect(user).to.be.instanceof(MarketerWatcherUser)
    })
})
