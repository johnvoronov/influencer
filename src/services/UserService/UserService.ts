import AbstractUser from "./models/AbstractUser";
import InfluencerUser from "./models/InfluencerUser";
import UnknownUserTypeError from "./errors/UnknownUserTypeError";
import UserRepositoryReader from "./interfaces/UserRepositoryReader";
import UserRepositoryWriter from "./interfaces/UserRepositoryWriter";
import MarketerUser from "./models/MarketerUser";
import NewUserData from "./interfaces/NewUserData";
import UserData from "./interfaces/UserData";
import MarketerWatcherUser from "./models/MarketerWatcherUser";
import ExistingUserFactory from "./factories/ExistingUserFactory";
import ExistingUser from "./models/ExistingUser";
import ExistingUserData from "./interfaces/ExistingUserData";


export default class UserService {
    constructor(
        protected readonly userRepositoryReader: UserRepositoryReader,
        protected readonly userRepositoryWriter: UserRepositoryWriter,
        protected readonly existingUserFactory: ExistingUserFactory,
        protected readonly config: any
    ) {

    }

    public async register(user: AbstractUser, password: string): Promise<void> {
        if (user instanceof InfluencerUser) {
            return this.registerInfluencer(user, password)
        }

        if (user instanceof MarketerUser) {
            return this.registerMarketer(user, password)
        }

        if (user instanceof MarketerWatcherUser) {
            return this.registerMarketerWatcher(user, password)
        }

        throw new UnknownUserTypeError(user)
    }

    public async getUserByCredentials(email: string, password: string): Promise<ExistingUser> {
        const existingUserData = await this.userRepositoryReader.getUserByCredentials(email, password)

        return this.existingUserFactory.create(existingUserData)
    }

    public async getUserById(userId: string): Promise<ExistingUser> {
        const existingUserData = await this.userRepositoryReader.getUserById(userId)

        return this.existingUserFactory.create(existingUserData)
    }

    public async getUserAvatarById(userId: string): Promise<any> {
        const existingUserData = await this.userRepositoryReader.getUserById(userId)

        const userData = await this.existingUserFactory.create(existingUserData)

        return {
            avatar: userData.user.avatar,
            avatarDescription: userData.user.avatarDescription
        }
    }

    public async updateUserAvatar(userId: string, avatar: string, avatarDescription: string): Promise<ExistingUser> {
        const userData = await this.userRepositoryReader.getUserById(userId)

        const existingUserData = await this.userRepositoryWriter
            .updateUser(userData.id, { ...userData, avatar, avatarDescription, })

        return this.existingUserFactory.create(existingUserData)
    }

    protected async registerInfluencer(user: InfluencerUser, password: string): Promise<void> {
        const data: NewUserData = {
            password,
            ...this.getCommonUserDataFromUser(user),
            birthDate: user.birthDate,
            gender: user.gender,
        }

        await this.userRepositoryWriter.saveUser(data)
    }

    protected async registerMarketer(user: MarketerUser, password: string): Promise<void> {
        const data: NewUserData = {
            password,
            ...this.getCommonUserDataFromUser(user),
            jobDescription: user.jobDescription,
            phoneNumber: user.phoneNumber,
        }

        await this.userRepositoryWriter.saveUser(data)
    }

    protected async registerMarketerWatcher(user: MarketerWatcherUser, password: string): Promise<void> {
        const data: NewUserData = {
            password,
            ...this.getCommonUserDataFromUser(user)
        }

        await this.userRepositoryWriter.saveUser(data)
    }

    protected getCommonUserDataFromUser(user: AbstractUser): UserData {
        return {
            type: user.type,
            email: user.email,
            marketer: user.marketer,
            firstName: user.firstName,
            lastName: user.lastName,
            avatar: user.avatar,
            avatarDescription: user.avatarDescription,
        }
    }
}
