import AbstractUser from "./AbstractUser";
import {USER_TYPES} from "../entities/enums";

export default class MarketerWatcherUser extends AbstractUser {
    readonly type = USER_TYPES.MARKETER_WATCHER

    constructor(
        email: string,
        public readonly marketer: string,
        public readonly firstName?: string,
        public readonly lastName?: string,
    ) {
        super(email, marketer)
    }
}
