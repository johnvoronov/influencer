import AbstractUser from "./AbstractUser";

export default class ExistingUser<TUser = AbstractUser> {
    constructor(
        public readonly userId: string,
        public readonly user: TUser
    ) {}
}
