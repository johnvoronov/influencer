import {USER_TYPES} from "../entities/enums";

export default abstract class AbstractUser {
    public abstract readonly type: USER_TYPES

    constructor(
        public readonly email: string,
        public readonly firstName?: string,
        public readonly lastName?: string,
        public readonly avatar?: string,
        public readonly avatarDescription?: string,
        public readonly marketer?: string,
    ) {}
}
