import AbstractUser from "./AbstractUser";
import {USER_TYPES} from "../entities/enums";

export default class MarketerUser extends AbstractUser {
    readonly type = USER_TYPES.MARKETER

    constructor(
        email: string,
        firstName: string,
        lastName: string,
        public readonly jobDescription?: string,
        public readonly phoneNumber?: string,
        public readonly avatar?: string,
        public readonly avatarDescription?: string
    ) {
        super(email, firstName, lastName)
    }
}
