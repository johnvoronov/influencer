import AbstractUser from "./AbstractUser";
import {GENDERS, USER_TYPES} from "../entities/enums";

export default class InfluencerUser extends AbstractUser {
    readonly type = USER_TYPES.INFLUENCER

    constructor(
        email: string,
        firstName: string,
        lastName: string,
        public readonly birthDate: Date,
        public readonly gender?: GENDERS,
        public readonly avatar?: string,
        public readonly avatarDescription?: string
    ) {
        super(email, firstName, lastName)
    }
}
