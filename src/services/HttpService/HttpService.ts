import * as Koa from "koa";
import * as Router from 'koa-router';
import * as json from 'koa-json';
import * as logger from 'koa-logger';
import * as bodyparser from 'koa-bodyparser'
import * as multer from 'koa-multer'
import * as path from 'path'
import {Server} from "http";
import RouteLoader from "./RouteLoader";
import RouteConstructParams from "./models/RouteConstructParams";

export default class HttpService {
    public async start(routeConstructParams: RouteConstructParams, port?: number): Promise<Server> {
        const app = new Koa();
        const router = new Router();



        app.use(json());
        app.use(logger());
        app.use(bodyparser())

        const loader = new RouteLoader<RouteConstructParams>(routeConstructParams)

        await loader.appendRoutesFromDir(router, `${__dirname}${path.sep}..${path.sep}..${path.sep}router`)
        app.use(router.routes())

        return await new Promise((resolve, reject) => {
            const server = app.listen(port, () => {
                resolve(server);
            });
        })
    }
}
