import {Stats} from 'fs';
import * as path from 'path';
import LoaderError from "./errors/LoaderError";
import * as Ajv from "ajv";
import InvalidInputError from "./models/InvalidInputError";
import AbstractResponse from "./responses/AbstractResponse";
import {Context} from "koa";
import * as Router from "koa-router";
import AbstractRoute from "./routes/AbstractRoute";
import JsonResponse from "./responses/JsonResponse";

const fs = require('fs');

type InvalidInputHandler = (err: InvalidInputError, ctx: Context) => Promise<AbstractResponse> | AbstractResponse;
type ErrorCatchHandler = (err: Error, ctx: Context) => Promise<AbstractResponse> | AbstractResponse;

const ajv = new Ajv({coerceTypes: true});

export type Options = {
    /**
     * If handler throw error and it's not caught on other levels, this is last option how to handle it.
     *
     * @param err
     */
    errorCatchHandler?: ErrorCatchHandler;

    /**
     * Invalid request to routes with defined validation schemas will be rejected through this response.
     */
    invalidInputHandler?: InvalidInputHandler;
}

export default class RouteLoader<TRouteConstructParams = any> {

    public constructor(
        public readonly routeConstructParams?: TRouteConstructParams,
        public readonly options: Options = {}
    ) {
    }

    /**
     * Append router passed in parameter by routes defined in specific directory.
     *
     * @param router
     * @param dirPath
     */
    async appendRoutesFromDir(router: Router, dirPath: string): Promise<Router> {
        const dirs: string[] = [];
        const files: string[] = [];

        const dirItems = (await this.getDirItems(dirPath))
            .filter((item) => ['.', '_'].indexOf(item[0]) === -1) // Skip items started with `_` or `.`
            .map((itemPath) => path.resolve(`${dirPath}/${itemPath}`)); // Modify item names to full path

        await this.separateDirItemsToTypes(dirItems, dirs, files);

        /**
         * When we initialize each route, we wanna skip its 'index', because there is not route, but router
         * decorator.
         */
        const dirRequireResolve = this.getDirResolver(dirPath);

        /**
         * Start routes loading
         */
        for (const filePath of files) {
            /**
             * We wanna skip index.js etc.
             */
            if (filePath === dirRequireResolve) {
                continue;
            }

            // Load route definition
            const route = await this.getRouteInstanceByPath(filePath);

            this.initRouteToRouter(router, route);
        }

        for (const dirPath of dirs) {
            await this.appendRoutesFromDir(router, dirPath);
        }

        return router;
    }

    /**
     * Add instance of Abstract route to router.
     *
     * @param router
     * @param {AbstractRoute} route
     * @private
     */
    protected initRouteToRouter(router: any, route: AbstractRoute) {
        const method = route.method.toLowerCase();

        router[method](route.path, ...this.getRouteMiddlewareArray(route), this.getRouteHandler(route));
    }

    /**
     * Require Route from file path.
     * It should be class extended AbstractRoute.
     *
     * @param routePath
     */
    protected async getRouteInstanceByPath(routePath: string): Promise<AbstractRoute> {
        const routeDefinition = await import(routePath);
        const initializer = routeDefinition.default || routeDefinition;

        let routeInstance;

        try {
            routeInstance = await (new initializer(this.routeConstructParams));
        } catch (err) {
            if (err.message.indexOf('is not a constructor') === -1) {
                throw err;
            }

            routeInstance = await initializer(this.routeConstructParams);
        }

        if (!(routeInstance instanceof AbstractRoute)) {
            throw new LoaderError(`Route "${routePath}' is not instance of AbstractRoute`);
        }

        return routeInstance;
    }

    /**
     * Load info for each item in directory and fill dir and files arrays in parameters.
     *
     * @param dirItems
     * @param dirs
     * @param files
     */
    protected async separateDirItemsToTypes(dirItems: string[], dirs: string[], files: string[]) {
        for (const dirItem of dirItems) {
            const stat = await this.getDirItemStats(dirItem);

            if (stat.isDirectory()) {
                dirs.push(dirItem);
            } else {
                files.push(dirItem);
            }
        }
    }

    /**
     * We need to get all items from directory.
     *
     * @param dirPath
     */
    protected getDirItems(dirPath: string): Promise<string[]> {
        return new Promise<string[]>((resolve, reject) => {
            fs.readdir(dirPath, (err: Error, items: string[]) => {
                if (err) {
                    return reject(err);
                }

                resolve(items);
            })
        })
    }

    /**
     * Return dir item stats base on path.
     *
     * @param fullPath
     */
    protected getDirItemStats(fullPath: string): Promise<Stats> {
        return new Promise<Stats>((resolve, reject) => {
            fs.stat(fullPath, (err: Error, stats: Stats) => {
                if (err) {
                    return reject(err);
                }

                resolve(stats);
            })
        })
    }


    /**
     * Return all middleware created to each route.
     *
     * @param route
     */
    protected getRouteMiddlewareArray(route: AbstractRoute): Function[] {
        const routePreMiddleware = route.preMiddleware || [];
        const routeMiddleware = route.middleware || [];

        const middlewareArray = [
            ...(Array.isArray(routePreMiddleware) ? routePreMiddleware : [routePreMiddleware])
        ];

        const dataSchema = route.bodySchema;
        const querySchema = route.querySchema;
        const parametersSchema = route.parametersSchema;

        if (dataSchema) {
            const getBodyFromRequest = (ctx: Context) => {
                return ctx.request.body || {};
            };

            middlewareArray.push(this.getValidationMiddleware(getBodyFromRequest, dataSchema, 'BODY'));
        }

        if (querySchema) {
            const getQueryFromRequest = (ctx: Context) => {
                return ctx.request.query;
            };

            middlewareArray.push(this.getValidationMiddleware(getQueryFromRequest, querySchema, 'QUERY'));
        }

        if (parametersSchema) {
            const getParametersFromRequest = (ctx: Context) => {
                return ctx.params;
            };

            middlewareArray.push(this.getValidationMiddleware(getParametersFromRequest, parametersSchema, 'PARAMETERS'));
        }

        return [
            ...middlewareArray,
            ...(Array.isArray(routeMiddleware) ? routeMiddleware : [routeMiddleware])
        ];
    }

    /**
     * Create middleware for testing validation json schema.
     *
     * @param requestGetterValue
     * @param schema
     * @param sectionName
     * @return {function(*=, *=, *)}
     */
    protected getValidationMiddleware(requestGetterValue: (ctx: Context) => {}, schema: object, sectionName: string): Function {
        const validate = ajv.compile(schema);

        return (ctx: Context, next: Function) => {
            const data = requestGetterValue(ctx);

            if (!validate(data)) {
                const error = new InvalidInputError(sectionName, validate.errors, schema);

                Promise.resolve(
                    (this.options.invalidInputHandler
                        ? this.options.invalidInputHandler
                        : this.defaultInvalidInputHandler)(error, ctx))
                    .then(async (response: AbstractResponse) => {
                        await response.sendToResponse(ctx);
                    });

                return;
            }

            next();
        }
    };

    protected getDirResolver(dirPath: string) {
        try {
            return require.resolve(dirPath)
        } catch (e) {
            return null;
        }
    }

    /**
     * Common handler for route.
     * it's called for every route and handle all necessary things before it's passed to handlers of each specific routes.
     *
     * @param route
     */
    protected getRouteHandler(route: AbstractRoute) {
        return async (ctx: Context) => {
            try {
                const response = await route.handler(ctx);

                return response.sendToResponse(ctx);
            } catch (e) {
                Promise.resolve(
                    (this.options.errorCatchHandler
                        ? this.options.errorCatchHandler
                        : this.defaultErrorCatchHandler)(e, ctx))
                    .then(async (response: AbstractResponse) => {
                        await response.sendToResponse(ctx);
                    });

                return;
            }
        }
    };

    protected async defaultErrorCatchHandler(error: Error, ctx: Context) {
        const body = {
            error: 'SERVER_ERROR',
            error_description: error.message,
            error_stack: error.stack
        };

        return new JsonResponse(body, 500);
    }

    protected async defaultInvalidInputHandler(error: InvalidInputError, ctx: Context) {
        const body = {
            error: 'INVALID_INPUT',
            error_description: `Input data in ${error.sectionName} section are wrong or missing`,
            error_validation: {
                errors: error.errors,
                schema: error.schema
            }
        };

        return new JsonResponse(body, 422);
    }
}
