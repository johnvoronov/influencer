import {Response} from "express";
import EmptyResponse from "./EmptyResponse";
import {Context} from "koa";

export default class PlainResponse extends EmptyResponse {
    constructor(
        protected readonly body: string = '',
        protected readonly statusCode: number = 200) {
        super(statusCode);
    }

    public async sendToResponse(ctx: Context): Promise<void> {
        await super.sendToResponse(ctx)
        ctx.body = this.body
    }
}