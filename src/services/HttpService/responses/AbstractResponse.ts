import {Response} from "express";
import {Context} from "koa";

export default abstract class AbstractResponse {
    /**
     * Sent response to response object passed from express.
     */
    abstract sendToResponse(ctx: Context): Promise<void>;
}