import {Response} from "express";
import AbstractResponse from "./AbstractResponse";
import {Context} from "koa";

export default class EmptyResponse extends AbstractResponse {
    constructor(
        protected readonly statusCode: number = 200
    ) {
        super()
    }

    async sendToResponse(ctx: Context): Promise<void> {
        ctx.status = this.statusCode
    }
}