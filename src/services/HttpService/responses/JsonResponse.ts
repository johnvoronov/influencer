import {Response} from "express";
import EmptyResponse from "./EmptyResponse";
import {Context} from "koa";

export default class JsonResponse extends EmptyResponse {
    constructor(
        protected readonly body: object = {}, statusCode: number = 200) {
        super(statusCode);
    }


    async sendToResponse(ctx: Context): Promise<void> {
        await super.sendToResponse(ctx)
        ctx.body = this.body
    }
}