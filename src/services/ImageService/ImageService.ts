import * as fs from "fs";
import ImageData from "./models/ImageData";
import MaximumImageSizeError from "./errors/MaximumImageSizeError";
import ImageServiceError from "./errors/ImageServiceError";

export default class ImageService {
    constructor(
        public readonly MAXIMUM_IMAGE_SIZE: number
    ) {
    }

    async saveImage(filename: string, imageDataBuffer: Buffer): Promise<ImageData> {
        if (imageDataBuffer.length > this.MAXIMUM_IMAGE_SIZE) {
            throw new MaximumImageSizeError(this.MAXIMUM_IMAGE_SIZE)
        }

        fs.writeFile(filename, imageDataBuffer, async (e: Error) => {
            if (e) {
                return new ImageServiceError()
            }
        })

        return new ImageData(
            Math.random().toString(36).substring(7),
            filename
        )
    }
}
