import ImageServiceError from "./ImageServiceError";

export default class ImageNotFoundError extends ImageServiceError {
    constructor(imageId: string) {
        super(`Image with id ${imageId} not found.`)

        Object.setPrototypeOf(this, ImageNotFoundError.prototype)
    }
}