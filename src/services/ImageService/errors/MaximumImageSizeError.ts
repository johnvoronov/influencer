import ImageServiceError from "./ImageServiceError";

export default class MaximumImageSizeError extends ImageServiceError {
    constructor(maximumSize: number) {
        super(`Maximum size is ${maximumSize}`)

        Object.setPrototypeOf(this, MaximumImageSizeError.prototype)
    }
}