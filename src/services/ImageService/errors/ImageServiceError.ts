export default class ImageServiceError extends Error {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, ImageServiceError.prototype)
    }
}