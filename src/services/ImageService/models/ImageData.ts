export default class ImageData {
    constructor(
        public readonly id: string,
        public readonly imageName: string,
    ){}
}