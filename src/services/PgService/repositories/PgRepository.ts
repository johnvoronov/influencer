import { Client } from "pg"
import * as changeCase from 'change-case'
import PgRepositoryWriter from "../interfaces/PgRepositoryWriter";
import PgRepositoryReader from "../interfaces/PgRepositoryReader";
import DbServiceError from "../errors/DbServiceError";
import DbDataIdNotFoundError from "../errors/DbDataIdNotFoundError";


export default class PgRepository implements PgRepositoryWriter, PgRepositoryReader  {
    protected readonly pgClient: Client
    protected readonly table: string

    constructor(table: string, config: any) {
        this.pgClient = new Client({
            host: config.postgres_host,
            port: config.postgres_port,
            user: config.postgres_user,
            password: config.postgres_password,
            database: config.postgres_db,
        })

        this.pgClient.connect()
        this.table = table
    }

    async saveData(data: object): Promise<any> {
        const { keys, values, indexes } = this.prepareData(data)

        const preparedKeys = this.convertKeysToRequest(keys)

        const response = await this.pgClient.query(`INSERT INTO ${this.table} (${preparedKeys.join(', ')}) VALUES(${indexes}) RETURNING *`, values);

        if (!response || !response.rows || !response.rows[0]) {
            throw new DbServiceError()
        }

        return (this.convertResponseToResult(response.rows))[0]
    }

    async updateData(id: string, data: object): Promise<any> {
        // Try to find data by id
        await this.getDataById(id);

        const keys: any = [];
        const values: any = (Object.values(data));
        const preparedKeys = this.convertKeysToRequest(Object.keys(data))

        Object
            .keys(preparedKeys)
            .map((key, index) => {
                keys.push(`${preparedKeys[key]} = $${index + 1}`);
            });


        const response = await this.pgClient.query(`UPDATE ${this.table} SET ${keys.join(', ')} WHERE id = ${id} RETURNING *`, values);

        if (!response || !response.rows || !response.rows[0]) {
            throw new DbServiceError()
        }

        return (this.convertResponseToResult(response.rows))[0]
    }

    async getDataByParams(data: object): Promise<any> {
        const keys: any = [];
        const { values } = this.prepareData(data)

        Object
            .keys(data)
            .map(function (key, index) {
                keys.push(`${key} = $${index + 1}`);
            });

        const response = await this.pgClient.query(`SELECT * FROM ${this.table} WHERE ${keys.join(' AND ')} ORDER BY created_at DESC;`, values);

        if (!response || !response.rows || !response.rows[0]) {
            return []
        }

        return this.convertResponseToResult(response.rows)
    }

    async getDataById(id: string): Promise<any> {
        const response = await this.pgClient.query(`SELECT * FROM ${this.table} WHERE id = $1;`, [id]);

        if (!response || !response.rows || !response.rows[0]) {
            throw new DbDataIdNotFoundError(id)
        }

        return (this.convertResponseToResult(response.rows))[0]
    }

    prepareData(data: object): any {
        const keys = (Object.keys(data));
        const values = (Object.values(data));
        const indexes: any  = [];

        keys.map((item: any, index: number) => {
            indexes.push(`$${index + 1}`)
        });

        return {
            keys,
            values,
            indexes
        }
    }

    convertKeysToRequest(keys: any) {
        return keys.map((key: any) => {
            return changeCase.snakeCase(key)
        })
    }

    convertResponseToResult(response: any) {
        return response.map((item: any) => {
            const data: any = Object.keys(item)
            const values: any = Object.values(item)

            const result: any = {}
            data.map((key: any, index: any) => {
                result[`${changeCase.camelCase(key)}`] = values[index]
            })

            return result
        })
    }
}
