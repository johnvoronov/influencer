import DbDataNotFoundError from "./DbDataNotFoundError";

export default class DbDataIdNotFoundError extends DbDataNotFoundError {
    constructor(id: string) {
        super(`Data with id "${id}" was not found`)

        Object.setPrototypeOf(this, DbDataIdNotFoundError.prototype)
    }
}
