export default class DbServiceError extends Error {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, DbServiceError.prototype)
    }
}
