import DbServiceError from "./DbServiceError";

export default class DbDataNotFoundError extends DbServiceError {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, DbServiceError.prototype)
    }
}
