export default interface PgRepositoryWriter {
    /**
     *
     * @param data
     */
    saveData(data: object): Promise<any>

    /**
     *
     * @param id
     * @param data
     */
    updateData(id: string, data: object): Promise<any>
}
