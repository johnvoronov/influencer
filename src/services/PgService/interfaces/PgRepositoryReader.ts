export default interface PgRepositoryReader {
    /**
     *
     * @param data
     */
    getDataByParams(data: object): Promise<any>

    /**
     *
     * @param id
     * @throws UserIdNotFoundError
     * @throws MessageRepositoryError
     */
    getDataById(id: string): Promise<any>
}
