import MessageRepositoryWriter from "../interfaces/MessageRepositoryWriter";
import MessageRepositoryReader from "../interfaces/MessageRepositoryReader";
import ExistingMessageData from "../interfaces/ExistingMessageData";
import MessageData from "../interfaces/MessageData";
import PgRepository from "../../PgService/repositories/PgRepository";
import MessageListData from "../interfaces/MessageListData";
import MessageCreationError from "../errors/MessageCreationError";
import MessageList from "../models/MessageList";


export default class MessageRepository implements MessageRepositoryWriter, MessageRepositoryReader   {
    protected readonly pgRepository: PgRepository

    constructor(config: any) {
        this.pgRepository = new PgRepository('messages', config)
    }

    public async saveMessage(messageData: MessageData): Promise<ExistingMessageData> {
        const response = await this.pgRepository.saveData(messageData)

        if (!response) {
            throw new MessageCreationError(messageData)
        }

        return response
    }

    public async getList(sender: string, recipient: string): Promise<MessageListData> {
        const messages = await this.pgRepository.getDataByParams({ pair: `${sender}-${recipient}` });

        return new MessageList(
            messages.length,
            100,
            messages
        )
    }
}
