import MessageServiceError from "./MessageServiceError";

export default class MessageCreationError extends MessageServiceError {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, MessageCreationError.prototype)
    }
}
