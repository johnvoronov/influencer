export default class MessageServiceError extends Error {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, MessageServiceError.prototype)
    }
}
