import MessageServiceError from "./MessageServiceError";

export default class MessageAccessDeniedError extends MessageServiceError {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, MessageAccessDeniedError.prototype)
    }
}
