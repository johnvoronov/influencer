import MessageServiceError from "./MessageServiceError";

export default class MessageMissingUserDataError extends MessageServiceError {
    constructor() {
        super(`Missing some or all fields in message data necessary for creating message`)

        Object.setPrototypeOf(this, MessageMissingUserDataError.prototype)
    }
}
