import MessageServiceError from "./MessageServiceError";

export default class MessageRepositoryError extends MessageServiceError {
    constructor(...args: any[]) {
        super(...args)

        Object.setPrototypeOf(this, MessageRepositoryError.prototype)
    }
}
