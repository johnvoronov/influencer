import MessageRepositoryReader from "./interfaces/MessageRepositoryReader";
import MessageRepositoryWriter from "./interfaces/MessageRepositoryWriter";
import ExistingMessageFactory from "./factories/ExistingMessageFactory";
import UserService from "../UserService/UserService";
import {USER_TYPES} from "../UserService/entities/enums";
import MessageAccessDeniedError from "./errors/MessageAccessDeniedError";
import MessageData from "./interfaces/MessageData";
import MessageListData from "./interfaces/MessageListData";
import JwtService from "../JwtService/JwtService";


export default class MessageService {
    constructor(
        protected readonly messageRepositoryReader: MessageRepositoryReader,
        protected readonly messageRepositoryWriter: MessageRepositoryWriter,
        protected readonly existingMessageFactory: ExistingMessageFactory,
        protected readonly userService: UserService,
        protected readonly jwtService: JwtService,
        protected readonly config: any
    ) {

    }

    public async send(messageData: MessageData): Promise<MessageData> {
        const isUserCanSendMessage = await this.canUserSendMessage(messageData.sender, messageData.recipient)

        if (isUserCanSendMessage) {
            return this.messageRepositoryWriter.saveMessage(messageData)
        }

        throw new MessageAccessDeniedError()
    }


    public async canUserSendMessage(sender: string, recipient: string): Promise<boolean> {
        const userData = await this.userService.getUserById(sender)

        if (userData.user.type === USER_TYPES.MARKETER) return true

        if (userData.user.type === USER_TYPES.INFLUENCER) {
            const messagesList = await this.messageRepositoryReader.getList(sender, recipient)
            if (messagesList.hits > 0) return true
        }

        return false
    }

    public async canUserWatchMessages(watcher: string, sender: string, recipient: string): Promise<boolean> {
        const userData = await this.userService.getUserById(watcher)

        if (userData.user.type === USER_TYPES.MARKETER_WATCHER) {
            if (userData.user.marketer === sender || userData.user.marketer === recipient) return true

            throw new MessageAccessDeniedError()
        }

        return false
    }

    public async getList(jwtToken: string, sender: string, recipient: string): Promise<MessageListData> {
        const { userId } = await this.jwtService.getJwtData(jwtToken)

        const isUserCanWatchMessages = this.canUserWatchMessages(userId, sender, recipient)

        if (isUserCanWatchMessages) {
            return await this.messageRepositoryReader.getList(sender, recipient)
        }

        throw new MessageAccessDeniedError()
    }
}
