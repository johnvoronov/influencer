import MessageFactory from "./MessageFactory";
import ExistingMessage from "../models/ExistingMessage";
import ExistingMessageData from "../interfaces/ExistingMessageData";

export default class ExistingMessageFactory {
    constructor(
        protected readonly messageFactory: MessageFactory
    ) {}

    public async create(messageData: ExistingMessageData): Promise<ExistingMessage> {
        return new ExistingMessage(
            messageData.id,
            await this.messageFactory.create(messageData)
        )
    }
}
