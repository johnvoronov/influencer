import Message from "../models/Message";
import ExistingMessageData from "../interfaces/ExistingMessageData";
import MessageMissingDataError from "../errors/MessageMissingDataError";

export default class MessageFactory {
    public async create(messageData: ExistingMessageData): Promise<Message> {
        const {sender, recipient, pair, text } = messageData


        if (!sender || !recipient || !pair || !text) {
            throw new MessageMissingDataError()
        }

        return new Message(
            sender,
            recipient,
            pair,
            text
        )
    }
}
