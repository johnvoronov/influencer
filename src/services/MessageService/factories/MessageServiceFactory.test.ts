import {expect} from 'chai'
import MessageServiceFactory from "./MessageServiceFactory";
import MessageService from "../MessageService";

describe('MessageServiceFactory', function () {
    it('should create MessageService', async function () {
        const FAKE_REPOSITORY: any = {}

        const factory = new MessageServiceFactory(FAKE_REPOSITORY, FAKE_REPOSITORY, {} as any, {} as any, {} as any, {} as any)

        const messageService = await factory.create()

        expect(messageService).to.be.instanceof(MessageService)
    })
})
