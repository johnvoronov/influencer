import {expect} from 'chai'
import ExistingMessageData from "../interfaces/ExistingMessageData";
import MessageFactory from "./MessageFactory";
import MessageMissingDataError from "../errors/MessageMissingDataError";
import Message from "../models/Message";

const FAKE_MESSAGE_DATA: ExistingMessageData = {
    id: '1',
    sender: '1',
    recipient: '2',
    pair: '1-2',
    text: 'Hello guys!'
}


describe('MessageFactory', function () {
    it('should create message', async function () {
        const factory = new MessageFactory()

        const message = await factory.create(FAKE_MESSAGE_DATA)

        expect(message).to.be.instanceof(Message)
    })

    it('should throw missing data error for influencer', async function () {
        const factory = new MessageFactory()

        const FAKE_MISSING_DATA = {
            ...FAKE_MESSAGE_DATA
        }

        delete FAKE_MISSING_DATA.sender

        try {
            const message = await factory.create(FAKE_MISSING_DATA)
        } catch (e) {
            expect(e).to.be.instanceof(MessageMissingDataError)
            return
        }

        throw new Error('should never happened')
    })
})
