import MessageRepositoryReader from "../interfaces/MessageRepositoryReader";
import MessageRepositoryWriter from "../interfaces/MessageRepositoryWriter";
import ExistingMessageFactory from "./ExistingMessageFactory";
import MessageService from "../MessageService";
import UserService from "../../UserService/UserService";
import JwtService from "../../JwtService/JwtService";

export default class MessageServiceFactory {
    constructor(
        protected readonly messageRepositoryReader: MessageRepositoryReader,
        protected readonly messageRepositoryWriter: MessageRepositoryWriter,
        protected readonly existingMessageFactory: ExistingMessageFactory,
        protected readonly userService: UserService,
        protected readonly jwtService: JwtService,
        protected readonly config: any
    ) {

    }

    async create(): Promise<MessageService> {
        return new MessageService(
            this.messageRepositoryReader,
            this.messageRepositoryWriter,
            this.existingMessageFactory,
            this.userService,
            this.jwtService,
            this.config
        )
    }
}
