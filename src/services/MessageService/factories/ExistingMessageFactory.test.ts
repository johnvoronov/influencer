import {expect} from 'chai'
import * as sinon from 'sinon'
import ExistingMessageData from "../interfaces/ExistingMessageData";
import ExistingMessage from "../models/ExistingMessage";
import ExistingMessageFactory from "./ExistingMessageFactory";

describe('ExistingMessageFactory', function () {
    it('should create ExistingMessage model from data', async function () {
        const FAKE_MESSAGE_DATA: ExistingMessageData = {
            id: '1',
            sender: '1',
            recipient: '2',
            pair: '1-2',
            text: 'Hello guys!'
        }

        const FAKE_MESSAGE_FACTORY: any = {
            create: sinon.spy(() => {
                return new ExistingMessage(
                    '1',
                    FAKE_MESSAGE_DATA
                )
            })
        }

        const factory = new ExistingMessageFactory(FAKE_MESSAGE_FACTORY)
        const existingMessage = await factory.create(FAKE_MESSAGE_DATA)

        expect(existingMessage.messageId).to.be.eql(FAKE_MESSAGE_DATA.id)
    })
})
