import {expect} from 'chai'
import * as sinon from 'sinon'
import MessageService from "./MessageService";
import MessageData from "./interfaces/MessageData";
import Message from "./models/Message";
import InfluencerUser from "../UserService/models/InfluencerUser";
import {GENDERS, USER_TYPES} from "../UserService/entities/enums";
import ExistingUser from "../UserService/models/ExistingUser";
import UserData from "../UserService/interfaces/UserData";
import UserService from "../UserService/UserService";
import AbstractUser from "../UserService/models/AbstractUser";
import UnknownUserTypeError from "../UserService/errors/UnknownUserTypeError";
import MessageAccessDeniedError from "./errors/MessageAccessDeniedError";
import MessageListData from "./interfaces/MessageListData";
import MessageList from "./models/MessageList";

describe('MessageService', function () {
    it ('should send the message correct from marketer to repository', async function () {
        const FAKE_MESSAGE_REPOSITORY_WRITER: any = {
            saveMessage: sinon.spy(async (messageData: MessageData) => {})
        }

        const FAKE_EXISTING_USER = new ExistingUser(
            '1',
             {
                type: USER_TYPES.MARKETER,
                email: 'john@doe.com',
                firstName: 'John',
                lastName: 'Doe',
                birthDate: new Date('1980-01-01'),
                gender: GENDERS.MAN
            }
        )

        const FAKE_USER_REPOSITORY_WRITER: any = {
            getUserById: sinon.spy(async (userId: string) => FAKE_EXISTING_USER)
        }

        const messageService = new MessageService({} as any, FAKE_MESSAGE_REPOSITORY_WRITER, {} as any, FAKE_USER_REPOSITORY_WRITER, {} as any, {} as any)

        const FAKE_MESSAGE = new Message('1', '2', '1-2', 'Hello world!')

        await messageService.send(FAKE_MESSAGE)

        expect(FAKE_MESSAGE_REPOSITORY_WRITER.saveMessage.calledOnce).to.be.eql(true)
        expect(FAKE_MESSAGE_REPOSITORY_WRITER.saveMessage.args[0][0].sender).to.be.eql('1')
    })

    it ('should throw error when try to send first message from influencer', async function () {
        const FAKE_MESSAGE_REPOSITORY_WRITER: any = {
            saveMessage: sinon.spy(async (messageData: MessageData) => {}),
        }

        const FAKE_MESSAGE_REPOSITORY_READER: any = {
            getList: sinon.spy(async (sender: string, recipient: string) => FAKE_MESSAGE_LIST)
        }

        const FAKE_EXISTING_USER = new ExistingUser(
            '1',
            {
                type: USER_TYPES.INFLUENCER,
                email: 'john@doe.com',
                firstName: 'John',
                lastName: 'Doe',
                birthDate: new Date('1980-01-01'),
                gender: GENDERS.MAN
            }
        )

        const FAKE_MESSAGE_LIST = new MessageList(
            0,
            100,
            [{
                sender: '2',
                recipient: '1',
                pair: '2-1',
                text: 'Hello, again!'
            }]
        )

        const FAKE_USER_REPOSITORY_WRITER: any = {
            getUserById: sinon.spy(async (userId: string) => FAKE_EXISTING_USER)
        }

        const messageService = new MessageService(FAKE_MESSAGE_REPOSITORY_READER, FAKE_MESSAGE_REPOSITORY_WRITER, {} as any, FAKE_USER_REPOSITORY_WRITER, {} as any, {} as any)

        const FAKE_MESSAGE = new Message('1', '2', '1-2', 'Hello world!')

        try {
            // @ts-ignore
            await messageService.send(FAKE_MESSAGE)
        } catch (e) {
            expect(e).to.be.instanceof(MessageAccessDeniedError)
            return
        }

        throw new Error('should never happened')
    })

    it ('should throw error when try to send first message from marketer watcher', async function () {
        const FAKE_MESSAGE_REPOSITORY_WRITER: any = {
            saveMessage: sinon.spy(async (messageData: MessageData) => {}),
        }

        const FAKE_MESSAGE_REPOSITORY_READER: any = {
            getList: sinon.spy(async (sender: string, recipient: string) => FAKE_MESSAGE_LIST)
        }

        const FAKE_EXISTING_USER = new ExistingUser(
            '1',
            {
                type: USER_TYPES.MARKETER_WATCHER,
                email: 'john@doe.com',
                firstName: 'John',
                lastName: 'Doe',
                birthDate: new Date('1980-01-01'),
                gender: GENDERS.MAN,
                watcher: 2
            }
        )

        const FAKE_MESSAGE_LIST = new MessageList(
            0,
            100,
            [{
                sender: '2',
                recipient: '1',
                pair: '2-1',
                text: 'Hello, again!'
            }]
        )

        const FAKE_USER_REPOSITORY_WRITER: any = {
            getUserById: sinon.spy(async (userId: string) => FAKE_EXISTING_USER)
        }

        const messageService = new MessageService(FAKE_MESSAGE_REPOSITORY_READER, FAKE_MESSAGE_REPOSITORY_WRITER, {} as any, FAKE_USER_REPOSITORY_WRITER, {} as any, {} as any)

        const FAKE_MESSAGE = new Message('1', '2', '1-2', 'Hello world!')

        try {
            // @ts-ignore
            await messageService.send(FAKE_MESSAGE)
        } catch (e) {
            expect(e).to.be.instanceof(MessageAccessDeniedError)
            return
        }

        throw new Error('should never happened')
    })

    it ('should get list of messages to marketer watcher to repository', async function () {
        const FAKE_MESSAGE_REPOSITORY_WRITER: any = {
            saveMessage: sinon.spy(async (messageData: MessageData) => {}),
        }

        const FAKE_JWT_SERVICE: any = {
            getJwtData: sinon.spy(async (jwtToken: string) => new Object({ userId: '3' })),
        }

        const FAKE_MESSAGE_REPOSITORY_READER: any = {
            getList: sinon.spy(async (sender: string, recipient: string) => FAKE_MESSAGE_LIST)
        }

        const FAKE_EXISTING_USER = new ExistingUser(
            '1',
            {
                type: USER_TYPES.MARKETER_WATCHER,
                email: 'john@doe.com',
                firstName: 'John',
                lastName: 'Doe',
                birthDate: new Date('1980-01-01'),
                gender: GENDERS.MAN
            }
        )

        const FAKE_MESSAGE_LIST = new MessageList(
            2,
            100,
            [{
                messageId: '1',
                message: {
                    sender: '2',
                    recipient: '1',
                    pair: '2-1',
                    message: 'Hello, again!'
                }
            }, {
                messageId: '1',
                message: {
                    sender: '2',
                    recipient: '1',
                    pair: '2-1',
                    message: 'Hello, I think marketer watcher is here!'
                }
            }]
        )

        const FAKE_JWT_TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIzIiwiaWF0IjoxNTY2NzY0MjczfQ.vjERu414aM3Y4zydxFsMU_ukvn-ORLTew5HV6NnDKn0'

        const FAKE_USER_REPOSITORY_WRITER: any = {
            getUserById: sinon.spy(async (userId: string) => FAKE_EXISTING_USER)
        }

        const messageService = new MessageService(FAKE_MESSAGE_REPOSITORY_READER, FAKE_MESSAGE_REPOSITORY_WRITER, {} as any, FAKE_USER_REPOSITORY_WRITER, FAKE_JWT_SERVICE, {} as any)

        await messageService.getList(FAKE_JWT_TOKEN, '2', '1')

        expect(FAKE_MESSAGE_REPOSITORY_READER.getList.calledOnce).to.be.eql(true)
        expect(await FAKE_MESSAGE_REPOSITORY_READER.getList.args[0][0]).to.be.eql('2')
    })
})
