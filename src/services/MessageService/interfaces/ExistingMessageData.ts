import MessageData from "./MessageData";

export default interface ExistingMessageData extends MessageData {
    readonly id: string
}
