import ExistingMessageData from "./ExistingMessageData";

export default interface MessageListData<TMessage = ExistingMessageData> {
    readonly hits: number,
    readonly offset: number
    readonly list: TMessage[]
}
