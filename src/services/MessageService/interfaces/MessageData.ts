export default interface MessageData {
    readonly sender: string,
    readonly recipient: string,
    readonly pair: string,
    readonly text: string
}
