import MessageListData from "./MessageListData";

export default interface MessageRepositoryReader {
    /**
     *
     * @param sender
     * @param recipient
     */
    getList(sender: string, recipient: string): Promise<MessageListData>
}
