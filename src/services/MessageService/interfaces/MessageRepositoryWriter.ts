import MessageData from "./MessageData";

export default interface MessageRepositoryWriter {
    /**
     *
     * @param messageData
     */
    saveMessage(messageData: MessageData): Promise<MessageData>
}
