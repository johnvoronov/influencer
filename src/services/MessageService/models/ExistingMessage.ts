import Message from "./Message";

export default class ExistingMessage<TMessage = Message> {
    constructor(
        public readonly messageId: string,
        public readonly message: TMessage
    ) {}
}
