export default class Message {
    constructor(
        public readonly sender: string,
        public readonly recipient: string,
        public readonly pair: string,
        public readonly text: string
    ) {}
}
