import ExistingMessage from "./ExistingMessage";

export default class MessageList<TMessage = ExistingMessage> {
    constructor(
        public readonly hits: number,
        public readonly offset: number,
        public readonly list: TMessage[]
    ) {}
}
