import {Server} from "http";
import kontik, {Services} from "kontik";
import enco from 'enco';
import HttpService from "./services/HttpService/HttpService";
import RouteConstructParams from "./services/HttpService/models/RouteConstructParams";
import UserServiceFactory from "./services/UserService/factories/UserServiceFactory";
import MessageServiceFactory from "./services/MessageService/factories/MessageServiceFactory";


export default class Application {
    getConfig(): any {
        return enco({
            dir: `${__dirname}/../config`,
            isFolderStructure: true
        });
    }

    getServices(config: any): Services {
        return kontik(config, {
            dir: `${__dirname}/providers`
        });
    }

    async start(): Promise<Server> {
        const config = this.getConfig();
        const services = this.getServices(config);

        const userServiceFactory = await services.getService<UserServiceFactory>('userServiceFactoryProvider')
        const userService = await userServiceFactory.create()

        const messageServiceFactory = await services.getService<MessageServiceFactory>('messageServiceFactoryProvider')
        const messageService = await messageServiceFactory.create()

        const httpService = await services.getService<HttpService>('httpServiceProvider')

        const routeConstructParams = new RouteConstructParams(
            services,
            config
        )

        return httpService.start(routeConstructParams, config.port)
    }
}
