create database 'influencer';
use influencer;

create sequence users_id_seq;

create table users(
    id int not null UNIQUE default nextval('users_id_seq'),
    marketer text,
    type text not null,
    email text not null,
    password text not null,
    first_name text,
    last_name text,
    avatar text,
    avatar_description text,
    birth_date date,
    gender text,
    job_description text,
    phone_number text,
    created_at timestamptz not null default now(),
    updated_at timestamptz not null default now(),
    UNIQUE (id),
    UNIQUE (email)
);

alter sequence users_id_seq
owned by users.id;

create sequence messages_id_seq;

create table messages(
    id int not null UNIQUE default nextval('messages_id_seq'),
    sender int not null,
    recipient int not null,
    pair text not null,
    message text not null,
    created_at timestamptz not null default now(),
    UNIQUE (id)
);

alter sequence messages_id_seq
owned by messages.id;
